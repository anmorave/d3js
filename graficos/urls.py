from django.contrib import admin
from django.urls import path
from graficos.views import *

urlpatterns = [
    path('barras/', barras),
    path('info', info, name = 'info'),
    path('tutorial/',tutorial, name = 'tutorial'),
    path('con_datos/',con_datos),
]
