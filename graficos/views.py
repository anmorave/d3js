import os
import pandas as pd
from django.shortcuts import render
from django.http import JsonResponse

# Create your views here.
def info(request):
    alf = pd.read_csv('alphabet.csv')
    alf = alf.to_dict('records')
    return JsonResponse(alf, safe = False)

def barras(request):
    print(os.getcwd())
    return render(request, 'barras.html')

def tutorial(request):
    return render(request,'formas_basicas.html')

def con_datos(request):
    return render(request, 'con_datos.html')